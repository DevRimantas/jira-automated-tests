package lt.telia.jira.tests.api;

public interface MyPluginComponent
{
    String getName();
}